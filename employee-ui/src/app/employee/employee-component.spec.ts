import { TestBed, fakeAsync, inject, waitForAsync } from '@angular/core/testing';
import {EmployeeComponent} from './employee-component';
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatToolbarModule} from '@angular/material/toolbar';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HttpClientModule} from '@angular/common/http';
import {DataTableModule} from 'angular-6-datatable';
import {service} from './service/employee-service';
import {TestData} from './test-data';
import {Observable} from 'rxjs';

describe('Component : Employee',() => {

  let comp: EmployeeComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations : [EmployeeComponent],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatIconModule,
        MatToolbarModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        FlexLayoutModule,
        MatTableModule,
        MatPaginatorModule,
        HttpClientModule,
        DataTableModule
      ],
      providers: [service],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  it('should create the app', () => {
    let fixture = TestBed.createComponent(EmployeeComponent);
    comp = fixture.debugElement.componentInstance;
    expect(comp).toBeTruthy();
  });

  it('retrieves all the cars', waitForAsync(inject( [service], ( empService:any ) => {
    empService.getEmployeeDetails().subscribe((result:any) => expect(result.length).toBeGreaterThan(0));
  })));
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSelectModule} from '@angular/material/select';
import {MatSortModule} from '@angular/material/sort';
import {MatDialogModule} from '@angular/material/dialog';
import { FlexLayoutModule } from '@angular/flex-layout';
import { EmployeeComponent } from './employee/employee-component';
import { service } from './employee/service/employee-service';
import { SaveEmployeeComponent } from './employee/save-employee/save-employee.component';
import { WelcomeComponent } from './welcome/welcome.component';
import {appRoutes} from './router';
import { DialogComponent } from './shared/dialog/dialog.component';
import { MessagesService } from './shared/messages-service/messages.service';
import { MessagesComponent } from './shared/messages-service/messages.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    SaveEmployeeComponent,
    WelcomeComponent,
    DialogComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RouterModule,
    MatCardModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FlexLayoutModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule,
    MatDialogModule,
    RouterModule.forRoot(appRoutes, { relativeLinkResolution: 'legacy' })
  ],
  providers: [service, MessagesService],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponent,MessagesComponent]
})
export class AppModule { }
 

# Employee Portal application using Angular2-springboot,java 8, JPA

# The following technology/framework required for this project<br>


**Server Side**

1. SpringBoot
2. Maven 3+ 
3. JAVA 8 
4. Spring MVC architecture
5. RestController
6. Spring jpa
7. H2 Database
8. Unit testing using junit


**Frontend side**

1. Angular2 Components<
2. Modal
3. CRUD Functionality (Create, Read(view/display))
4. Single Page Application
5. Routing | Router outlet | Router
6. Angular Material

# How to Setup this project 

**Backend**

1. Maven 3+
2. Java 8
3. Use spring initializer to use spring boot template with dependencies

**Frontend**
Clone project, Open cmd or Node js Command prompt

1. Run `npm install -g angular-cli ` to install angular 2 cli ( globally ) 
2. Run `npm install` to install Node packages


# How to Run >

**Backend**

1. Run `mvn clean install` to clean if exists files and install packages
2. Run `mvn spring-boot:run`  to start spring boot

**Frontend**

Run `ng serve --open` or `npm start`



*Make sure backend service is up and running before start frontend*

**Guide to Build and Run Project**

When you Import this project, You need to fulfill the requirements.

**If you have imported project in eclipse, Follow this steps to build project:**
1. Right click on project > Properties > **Java Build path**
2. Change your **JDK if 1.8 is not set.**
3. You need **Maven 3+** configured in your system (or You can download Maven 3+ from [here](https://maven.apache.org/download.cgi))
4. Once you setup with all this things,
Right click on project > Run as > Maven clean (you can directly do this using **mvn clean** command in cmd at specific project dir)
5. Again right click on project, Find **Maven >** in options, Click on **Update project**
check **Update snapshots forcefully** option and press ok.
6. Now you can refresh your project. Run clean and build project.
7. **Main Important step >** Right click on project > Run as  > **Maven Install**
 This step will install required dependency specified in [pom.xml]


**You may have question that how this was generated ?**
If you observed libraries (Dependencies) in pom.xml ,
```
<dependency>
	<groupId>com.h2database</groupId>
	<artifactId>h2</artifactId>
	<scope>runtime</scope>
</dependency>
```
You need to know more about [H2 Database](https://en.wikipedia.org/wiki/H2_(DBMS)). 
H2 is a relational database management system written in Java. It can be embedded in Java applications or run in the client-server mode.
- Very fast, open source, JDBC API
- Embedded and server modes; in-memory databases (Simple meaning => data will not persist on the disk)
- lightweight Java database
- It can be embedded in Java applications or run in the client-server mode
Whatever things (Objects, String or anything) you want to store in **H2** you can.
In this example, whatever values you want to store (CRUD operations), it will be retain in H2 Database till you shutdown your tomcat.(or other servers).

# Steps to run application using Docker

**For Employee Service**

1. Create docker image
	`docker build -t employee-service:1.0 .`
2. Run application using docker
	 `docker run -d -it -p 8080:8080/tcp --name=employee-service employee-service:1.0`
3. If applicaiton is already running,  to stop and remove from docker use the below commands respectively

	`docker stop employee-service`
	`docker rm employee-service`


**For Employee UI**

1. Create docker image
	`docker build -t employee-ui:1.0 .`
2. Run application using docker
	 `docker run -d -it -p 8081:80/tcp --name=employee-ui employee-ui:1.0`
3. If applicaiton is already running,  to stop and remove from docker use the below commands respectively

	`docker stop employee-ui`
	`docker rm employee-ui`



package com.sg.employee.controller.integration.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import com.sg.employee.dto.EmployeeDTO;
import com.sg.employee.enums.Gender;
import com.sg.employee.test.AbstractTest;

public class EmployeeControllerIntergrationTest extends AbstractTest {

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
	}

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	@Order(2)
	public void testAllEmployees() 
	{
		assertTrue(
				this.restTemplate
					.getForObject("http://localhost:" + port + "/api/v1/employee", List.class)
					.size() > 0);
	}

	@Test
	@Order(1)
	public void testAddEmployee() {
		EmployeeDTO employee = new EmployeeDTO ();
		employee.setFirstName("Unit");
		employee.setLastName("Test");
		employee.setGender(Gender.Male);
		employee.setDepartment("IT");
		employee.setDob(new Date());
		ResponseEntity<String> responseEntity = this.restTemplate
			.postForEntity("http://localhost:" + port + "/api/v1/employee", employee, String.class);
		
		assertEquals(200, responseEntity.getStatusCodeValue());
	}
	@Test
	@Order(3)
	public void testGetEmployeeByDept () {
		assertTrue(
				this.restTemplate
					.getForObject("http://localhost:" + port + "/api/v1/employee/IT", List.class)
					.size() == 1);
	}
	
	@Test
	@Order(4)
	public void deleteEmployee() throws URISyntaxException {
		
		final ResponseEntity<List<EmployeeDTO>> empList = this.restTemplate.exchange("http://localhost:" + port + "/api/v1/employee", HttpMethod.GET, null, new ParameterizedTypeReference<List<EmployeeDTO>>() {});
		assertTrue(empList.getBody().size() > 0);
		for ( EmployeeDTO emp :  empList.getBody()) {
		RequestEntity<String> requestEntity = new RequestEntity<>(HttpMethod.DELETE, new URI("http://localhost:" + port + "/api/v1/employee/"+ emp.getId()));
		ResponseEntity<String> responseEntity = this.restTemplate
				.exchange(requestEntity, String.class);
			
		assertEquals(200, responseEntity.getStatusCodeValue());
		}
	}

	
	
	
}
package com.sg.employee.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sg.employee.dto.EmployeeDTO;
import com.sg.employee.service.EmployeeService;

/**
 * Controller exposes employee APIs
 * 
 * @author Kausik Jana
 * @version 1.0
 *
 */

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {

	private static final Logger log = LoggerFactory.getLogger(EmployeeController.class);

	@Autowired
	EmployeeService employeeService;

	/**
	 * retrieves and returns list of employees
	 * 
	 * @return a list of employees
	 */
	@GetMapping ("/employee")
	public List<EmployeeDTO> getAllEmployeeDetails() {
		return employeeService.getAllEmployees();
	}

	/**
	 * add employee to the protal
	 * 
	 * @param employee
	 * @return {@link EmployeeDTO}
	 */
	@PostMapping ("/employee")
	@ResponseStatus(HttpStatus.OK)
	public EmployeeDTO createEmployee(@RequestBody EmployeeDTO employee) {
		log.info("Received employee object - {}",  employee);
		EmployeeDTO emp = null;
		if(Optional.ofNullable(employee).isPresent()) {
			emp = employeeService.createEmployee(employee);
		}
		return emp;
	}
	
	/**
	 * deletes employee for provided employee id
	 * 
	 * @param employee
	 * @return
	 * @throws Exception 
	 */
	@DeleteMapping ("/employee/{id}")
	@ResponseStatus(HttpStatus.OK)
	public String deleteEmployee(@PathVariable long id) throws Exception {
		log.info("Received employee id: {}",  id);
		if(Optional.ofNullable(id).isPresent()) {
			employeeService.deleteEmployee(id);
		}
		EmployeeDTO emp = employeeService.findByEmployeeId(id);
        if (emp == null) {
            return "Sorry, Employee not found!";
        }
        else{ 
    	employeeService.deleteEmployee(id); 
    	return "Employee deleted!";
        }
    }
	
	@GetMapping ("/employee/{departmentName}")
	public List<EmployeeDTO> getEmployeeByDept (@PathVariable String departmentName ){
		List<EmployeeDTO> empList = new ArrayList<>();
		if ( null != departmentName && !departmentName.isEmpty()) {
			empList = employeeService.getEmployeeByDept(departmentName);
		}
		
		return empList;
		
	}
	
	
}

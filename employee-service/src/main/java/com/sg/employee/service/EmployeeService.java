package com.sg.employee.service;

import java.util.List;

import com.sg.employee.dto.EmployeeDTO;

public interface EmployeeService {

	public List<EmployeeDTO> getAllEmployees();

	public EmployeeDTO createEmployee(EmployeeDTO empployeeDTO);

	public void deleteEmployee(long id);

	public EmployeeDTO findByEmployeeId(long employeeId) throws Exception;
	
	public List<EmployeeDTO> getEmployeeByDept(String departmentName);
}

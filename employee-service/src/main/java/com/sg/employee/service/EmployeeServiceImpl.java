package com.sg.employee.service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sg.employee.dto.EmployeeDTO;
import com.sg.employee.model.Employee;
import com.sg.employee.repository.EmployeeRepository;

/*--service class to implement business logic --*/

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepo;
	
	@Override
	public List<EmployeeDTO> getAllEmployees() {
		List<Employee> employeeList = employeeRepo.findAll();
		List<EmployeeDTO> employeeDTOList = employeeList.stream().map(this::covertToDTO)
		          .collect(Collectors.toList()); 
		employeeDTOList.sort(Comparator.comparing(EmployeeDTO::getFirstName));
		
		return employeeDTOList;
	}

	@Override
	public EmployeeDTO createEmployee(EmployeeDTO empployeeDTO) {
		Employee employee =  null;
		if(Optional.ofNullable(empployeeDTO).isPresent()) {
			employee = employeeRepo.save(covertToEntity(empployeeDTO));
		}
		return covertToDTO(employee);
		
	}
	
	@Override
	public void deleteEmployee(long employeeId) {
		Employee employee =  null;
		ModelMapper modelMapper = new ModelMapper();
		if(Optional.ofNullable(employeeId).isPresent()) {
			employeeRepo.deleteById(employeeId);
		}
		
	}
	
	@Override
	public EmployeeDTO findByEmployeeId(long employeeId) throws Exception {
			Optional<Employee> emp = employeeRepo.findById(employeeId);
			if ( null != null )
				return covertToDTO(emp.get());
			return null;
	}
	
	private EmployeeDTO covertToDTO (Employee employee){
		ModelMapper mapper = new ModelMapper();
		return mapper.map(employee, EmployeeDTO.class);
		
	}
	
	private Employee covertToEntity (EmployeeDTO employeeDTO){
		ModelMapper mapper = new ModelMapper();
		return mapper.map(employeeDTO, Employee.class);
		
	}

	@Override
	public List<EmployeeDTO> getEmployeeByDept(String departmentName) {
		List<EmployeeDTO> empDTOList = null ;
		List<Employee> empList =    employeeRepo.findAll();
		
		if ( null != empList && !empList.isEmpty())
			empDTOList =   empList.stream().filter(emp -> emp.getDepartment().equals(departmentName)).map(this::covertToDTO).collect(Collectors.toList());
		 
		
		return empDTOList;
	}
}
